# PHP Developer Application

Use this project to start applying for the job PHP Developer with Merchandise Essentials. All information is included in the README-file.

## Installation steps

This project was installed through the installation command `composer create-project --prefer-dist laravel\laravel php-developer`.
There are some basic views, routes and migrations associated with this project to give you a head start.

## Before you start

1.  Create a new branch using your snake-cased full name.
2.  Configure a simple database using SQLite - make sure it's included in your future commits.
(e.g. https://www.codementor.io/@goodnesskay/developing-locally-using-sqlite-with-laravel-on-ubuntu-8s8358503)
3.  Develop a page where a user can request a present. The state of the present is automatically changed from DRAFT to REQUESTED.
4.  A manager receives the notification and is able to approve or deny the request and provide a small explanation.
The state of the present is automatically changed to APPROVED or DENIED according to the manager’s choice.
5.  The requester receives the notification of the approval or denial and can actually send the present or cancel the request.
6.  Make it fully responsive and mobile friendly to earn some additional credits.
7.  You’re free to finish the complete project with your own style.
8.  Submit your branch and create a [Merge Request](https://gitlab.com/merchandise-essentials/applications/php-developer/-/commits/master).

## Guidelines

Following guidelines is key for teamwork. You're free to use external sites and guides, although it's appreciated if you include a list of the
specific guides or resources used in this project.

Do include

*  best practices applied (e.g. https://www.laravelbestpractices.com/, ...)
*  themes and layouts used (e.g. https://getbootstrap.com/docs/4.3/getting-started/introduction/, ...)
*  ...

to structure your sources.

No need to include sources like
* Google
* Github
* Stackoverflow

## Feedback and questions

Feel free to ask for feedback during implementation or after applying.

Email: bartel@merchandise-essentials.com
