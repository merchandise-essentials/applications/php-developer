<?php

namespace App\Http\Controllers;

use App\Http\Controllers\BaseController;

class Controller extends BaseController
{
    public function home() {
      return view('home');
    }
    
    public function welcome() {
      return view('welcome');
    }
}
